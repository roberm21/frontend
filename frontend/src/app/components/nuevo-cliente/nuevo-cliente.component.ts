import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-nuevo-cliente',
  templateUrl: './nuevo-cliente.component.html',
  styleUrls: ['./nuevo-cliente.component.css']
})
export class NuevoClienteComponent implements OnInit {

  public form: FormGroup;
  public clientId: any;

  constructor(private formBuilder: FormBuilder, private __apiService: ApiService, private __activateRoute: ActivatedRoute, private router: Router) {
    this.clientId = this.__activateRoute.snapshot.params.id
    if(this.clientId){
      this.__apiService.cliente(this.clientId).subscribe(result => {
        console.log(result)
        this.form.setValue(result.response)
      })
    }
    this.form = formBuilder.group({
      'clienteId': [null],
      'nombre': [null, Validators.compose([Validators.required])],
      'apellidoPaterno': [null, Validators.compose([Validators.required])],
      'apellidoMaterno': [null, Validators.compose([Validators.required])],
      'rfc': [null, Validators.compose([Validators.required])]
    });

  }

  ngOnInit() {
  }

  cancelar(){
    var confir = confirm("¿Deseas Salir?");
    if (confir) {
      this.router.navigateByUrl("clientes");
    }
  }

  guardar($ev: any): void {
    console.log("GUARDAR")
    $ev.preventDefault();
    for (let c in this.form.controls) {
      this.form.controls[c].markAsTouched();
    }
    if (this.form.valid) {
      if (this.clientId) {
        console.log(this.form.value)
        this.__apiService.actualizarCliente(this.form.value).subscribe(result => {
          console.log("Cliente actualizado -->",result)
          if (result.success) {
            alert("Cliente Actualizado")
            this.router.navigateByUrl("clientes");
          } else {
            alert("Error")
          }
        })
      } else {
        this.__apiService.crearCliente(this.form.value).subscribe(result => {
          console.log(result)
          if (result.success) {
            this.form.reset();
            alert("Cliente Guardado")
          } else {
            alert("Error")
          }
        })
      }
    }
  }

}
