import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {

  public clientes: any = []
  public articulos: any = []
  public articuloSelected: any;
  public articulosSelected: any = []
  public usuarioSelected: any = { rfc: "" };
  public configuraciones: any;
  public total: any = 0;
  public enganche: any = 0;
  public bonificacion: any = 0;
  public precioContado: any = 0;
  public abonosMensuales: any = [];
  public mensualidadSelected: any;
  public plazoSelected: any; 

  constructor(private __apiService: ApiService, private router: Router) {
    this.configuraciones = JSON.parse(localStorage.getItem("CONFIG"))
    this.__apiService.clientes().subscribe(result => {
      let arrayAux = result.response
      for (let i = 0; i < arrayAux.length; i++) {
        arrayAux[i].label = result.response[i].clienteId + ' - ' + result.response[i].nombre
      }
      this.clientes = arrayAux
    })
    this.__apiService.articulos().subscribe(result => {
      console.log("RESULT -->", result)
      if (result.success) {
        this.articulos = result.response
      }

    })
  }

  selectUsuario(event) {
    console.log("event -->", event)
    if (event) {
      this.usuarioSelected = event
    } else {
      this.usuarioSelected = { rfc: "" }
    }
  }
  selectArticulo(event) {
    console.log("event -->", event)
    if (event) {
      event.cantidad = 1
      event.precio = Number(event.precio * (1 + (this.configuraciones.tasa * this.configuraciones.plazo) / 100)).toFixed(0)
      event.importe = 1 * event.precio
      this.articuloSelected = event
    }
  }
  changeCantidad(i) {
    if (this.articulosSelected[i].cantidad <= this.articulosSelected[i].existencia) {
      this.articulosSelected[i].importe = this.articulosSelected[i].cantidad * this.articulosSelected[i].precio
      this.calcular()
    } else {
      alert("La cantidad supera el inventario actual del producto")
    }
  }
  addArticulo() {
    let flag = false;
    for (let i = 0; i < this.articulosSelected.length; i++) {
      if (this.articulosSelected[i].articuloId == this.articuloSelected.articuloId) {
        flag = true
      }
    }
    if (flag) {
      alert("Ya se agrego este articulo")
    } else {
      if (this.articuloSelected) {
        this.articulosSelected.push(this.articuloSelected)
        this.calcular()
      } else {
        alert("No has seleccionado articulo")
      }
    }
  }
  calcular() {
    let totalImporte = 0
    for (let i = 0; i < this.articulosSelected.length; i++) {
      totalImporte += this.articulosSelected[i].importe
    }
    console.log(totalImporte)
    this.enganche = Number((this.configuraciones.enganche / 100) * totalImporte).toFixed(2)
    this.bonificacion = Number(this.enganche * ((this.configuraciones.tasa * this.configuraciones.plazo) / 100)).toFixed(2)
    this.total = Number(totalImporte - this.enganche - this.bonificacion).toFixed(2)
  }
  eliminar(i) {
    this.articulosSelected.splice(i, 1)
    this.calcular()
  }

  siguiente() {
    if (this.usuarioSelected.nombre && this.articulosSelected.length > 0) {
      this.precioContado = Number(this.total / (1 + ((this.configuraciones.tasa * this.configuraciones.plazo) / 100))).toFixed(2)
      console.log("precio de contado -->", this.precioContado)
      let plazo = 0
      for (let i = 0; i < 4; i++) {
        plazo += 3
        this.abonosMensuales.push({ mensualidad: plazo, totalPagar: Number(this.precioContado * (1 + ((this.configuraciones.tasa * plazo) / 100))).toFixed(2), abono: Number((this.precioContado * (1 + ((this.configuraciones.tasa * (plazo)) / 100))) / plazo).toFixed(2), ahorro: Number(this.total - (this.precioContado * (1 + ((this.configuraciones.tasa * plazo) / 100)))).toFixed(2), mensualidadSelected: false })
      }
      console.log("abonos mesuales -->", this.abonosMensuales)
    } else {
      alert("Tienes que seleccionar un usuario y agregar al menos un articulo para poder continuar")
    }
  }
  selectMesualidad(i,item){
    console.log(i)
    this.abonosMensuales[i].mensualidadSelected = true
    this.plazoSelected = item;
    console.log("Se cambio la mensualidad")
  }

  guardar(){
    if (this.plazoSelected) {
      this.__apiService.crearVenta({clienteId: this.usuarioSelected.clienteId, nombre: this.usuarioSelected.nombre, total: this.plazoSelected.totalPagar, fecha: new Date().toISOString().slice(0, 19).replace('T', ' ')}).subscribe(result => {
        console.log("RESULT -->",result)
        if (result.success) {
          alert(result.response)
          this.router.navigateByUrl("/ventasList")
        }else{
          alert(result.response)
        }
      })
    }else{
      alert("Debe seleccionar un plazo para realizar el pago de su compra.")
    }
  }

  cancelar(){
    var confir = confirm("¿Deseas Salir?");
    if (confir) {
      this.router.navigateByUrl("ventas");
    }
  }
  ngOnInit() {
    if (!this.configuraciones) {
      alert("No cuentas con datos de configuración para continuar")
      this.router.navigateByUrl("configuracion");
    }
  }

}
