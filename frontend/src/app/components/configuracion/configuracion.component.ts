import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.css']
})
export class ConfiguracionComponent implements OnInit {

  public form: FormGroup

  constructor(private formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      'tasa': [null, Validators.compose([Validators.required])],
      'enganche': [null, Validators.compose([Validators.required])],
      'plazo': [null, Validators.compose([Validators.required])],
    });
    if (localStorage.getItem("CONFIG")) {
      this.form.setValue(JSON.parse(localStorage.getItem("CONFIG")))
    }
  }

  ngOnInit() {
  }

  guardar($ev: any): void {
    $ev.preventDefault();
    for (let c in this.form.controls) {
      this.form.controls[c].markAsTouched();
    }
    if (this.form.valid) {
      localStorage.setItem("CONFIG", JSON.stringify(this.form.value))
      alert("Datos de configuración guardados")
    }
  }

  cancelar() {
    var confir = confirm("¿Deseas Salir?");
    if (confir) {

    }
  }
}
