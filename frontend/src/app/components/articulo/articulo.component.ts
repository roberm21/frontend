import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css']
})
export class ArticuloComponent implements OnInit {

  public articulos: any = []
  constructor(private __apiService: ApiService) { }

  ngOnInit() {
    this.__apiService.articulos().subscribe(result => {
      console.log(result)
      if (result.success) {
        this.articulos = result.response
      }
    })
  }

}
