import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-ventas-list',
  templateUrl: './ventas-list.component.html',
  styleUrls: ['./ventas-list.component.css']
})
export class VentasListComponent implements OnInit {

  public ventas: any = []

  constructor(private __apiService: ApiService) {
    this.__apiService.ventas().subscribe(result => {
      console.log(result)
      this.ventas = result.response
    })
   }

  ngOnInit() {
  }

}
