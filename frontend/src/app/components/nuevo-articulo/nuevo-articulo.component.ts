import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-nuevo-articulo',
  templateUrl: './nuevo-articulo.component.html',
  styleUrls: ['./nuevo-articulo.component.css']
})
export class NuevoArticuloComponent implements OnInit {

  public form: FormGroup;
  public articuloId: any;

  constructor(private formBuilder: FormBuilder, private __apiService: ApiService, private __activateRoute: ActivatedRoute, private router: Router) {
    this.articuloId = this.__activateRoute.snapshot.params.id
    if (this.articuloId) {
      this.__apiService.articulo(this.articuloId).subscribe(result => {
        console.log(result)
        this.form.setValue(result.response)
      })
    }
    this.form = formBuilder.group({
      'articuloId': [null],
      'descripcion': [null, Validators.compose([Validators.required])],
      'modelo': [null, Validators.compose([Validators.required])],
      'precio': [null, Validators.compose([Validators.required])],
      'existencia': [null, Validators.compose([Validators.required])]
    });

  }

  ngOnInit() {
  }

  cancelar(){
    var confir = confirm("¿Deseas Salir?");
    if (confir) {
      this.router.navigateByUrl("articulos");
    }
  }

  guardar($ev: any): void {
    console.log("GUARDAR")
    $ev.preventDefault();
    for (let c in this.form.controls) {
      this.form.controls[c].markAsTouched();
    }
    if (this.form.valid) {
      if (this.articuloId) {
        console.log(this.form.value)
        this.__apiService.actualizarArticulos(this.form.value).subscribe(result => {
          console.log("Articulo actualizado -->", result)
          if (result.success) {
            this.router.navigateByUrl("articulos");
            alert("Articulo Actualizado")
          } else {
            alert("Error")
          }
        })
      } else {
        this.__apiService.crearArticulos(this.form.value).subscribe(result => {
          console.log(result)
          if (result.success) {
            this.form.reset();
            alert("Articulo Guardado")
          } else {
            alert("Error")
          }
        })
      }
    }
  }

}
