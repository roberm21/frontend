import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  public clientes: any = [];

  constructor(private __apiService: ApiService) {
  }

  ngOnInit() {
    this.__apiService.clientes().subscribe(result => {
      console.log(result)
      if (result.success) {
        this.clientes = result.response
      }
    })
  }

}
