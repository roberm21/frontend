import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { NuevoClienteComponent } from './components/nuevo-cliente/nuevo-cliente.component';
import { NuevoArticuloComponent } from './components/nuevo-articulo/nuevo-articulo.component';
import { ArticuloComponent } from './components/articulo/articulo.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { ConfiguracionComponent } from './components/configuracion/configuracion.component';
import { VentasComponent } from './components/ventas/ventas.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { VentasListComponent } from './components/ventas-list/ventas-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientesComponent,
    NuevoClienteComponent,
    NuevoArticuloComponent,
    ArticuloComponent,
    ConfiguracionComponent,
    VentasComponent,
    VentasListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgSelectModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
