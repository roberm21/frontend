import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientesComponent } from './components/clientes/clientes.component';
import { ArticuloComponent } from './components/articulo/articulo.component';
import { NuevoArticuloComponent } from './components/nuevo-articulo/nuevo-articulo.component';
import { NuevoClienteComponent } from './components/nuevo-cliente/nuevo-cliente.component';
import { ConfiguracionComponent } from './components/configuracion/configuracion.component';
import { VentasComponent } from './components/ventas/ventas.component';
import { VentasListComponent } from './components/ventas-list/ventas-list.component';

const routes: Routes = [
  {path: 'nuevaVenta', component: VentasComponent},
  {path: 'ventas', component: VentasListComponent},
  {path: 'clientes', component: ClientesComponent},
  {path: 'articulos', component: ArticuloComponent},
  {path: 'nuevoArticulo', component: NuevoArticuloComponent},
  {path: 'editarArticulo/:id', component: NuevoArticuloComponent},
  {path: 'nuevoCliente', component: NuevoClienteComponent},
  {path: 'editarCliente/:id', component: NuevoClienteComponent},
  {path: 'configuracion', component: ConfiguracionComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }

