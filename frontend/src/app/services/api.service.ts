import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Uris } from './Uris';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  sucursalId: any;
  user: any = {email: null, password: null};
  constructor(private httpClient: HttpClient) { }

  articulos() {
    return this.httpClient.get<any>(`${Uris.listaArticulos}`).pipe(
      catchError(this.handleError(`articulos error`))
    )
  }
  crearArticulos(obj) {
    return this.httpClient.post<any>(`${Uris.articulo}`,obj).pipe(
      catchError(this.handleError(`clientes error`))
    )
  }
  actualizarArticulos(obj) {
    return this.httpClient.put<any>(`${Uris.articulo}`,obj).pipe(
      catchError(this.handleError(`clientes error`))
    )
  }
  articulo(id){
    return this.httpClient.get<any>(`${Uris.articulo}/${id}`).pipe(
      catchError(this.handleError(`clientes error`))
    )
  }
  clientes() {
    return this.httpClient.get<any>(`${Uris.listaClientes}`).pipe(
      catchError(this.handleError(`clientes error`))
    )
  }
  crearCliente(obj) {
    return this.httpClient.post<any>(`${Uris.cliente}`,obj).pipe(
      catchError(this.handleError(`clientes error`))
    )
  }
  actualizarCliente(obj) {
    return this.httpClient.put<any>(`${Uris.cliente}`,obj).pipe(
      catchError(this.handleError(`clientes error`))
    )
  }
  cliente(id){
    return this.httpClient.get<any>(`${Uris.cliente}/${id}`).pipe(
      catchError(this.handleError(`clientes error`))
    )
  }
  ventas(){
    return this.httpClient.get<any>(`${Uris.ventas}`).pipe(
      catchError(this.handleError(`clientes error`))
    )
  }
  crearVenta(obj){
    return this.httpClient.post<any>(`${Uris.venta}`,obj).pipe(
      catchError(this.handleError(`clientes error`))
    )
  }
  
  

  

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
