export class Uris {
  /**
   * Url base de la api, con tan solo cambiar esta url funcionara la api.
   */
  
  public static API_ENDPOINT = "http://localhost:3000/api";
  
  public static listaClientes = `${Uris.API_ENDPOINT}/clientes`
  public static listaArticulos = `${Uris.API_ENDPOINT}/articulos`
  public static cliente = `${Uris.API_ENDPOINT}/cliente`
  public static articulo = `${Uris.API_ENDPOINT}/articulo`
  public static venta = `${Uris.API_ENDPOINT}/venta`
  public static ventas = `${Uris.API_ENDPOINT}/ventas`
}